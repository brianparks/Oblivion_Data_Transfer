package fish1062.UsernameProject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
/**
 * The class that implements Listener and listens for when a player joins to determine if data must be transfered from one person to another
 *
 */
public class PlayerListener implements Listener {
	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		boolean hasPlayedBefore = checkForPastLogin(player.getDisplayName());

		if (hasPlayedBefore) {
		} else {
			try {
				String UUID = URLConnectionReader("https://api.mojang.com/users/profiles/minecraft/"
						+ player.getDisplayName() + "?at=" + getUnixTimeStamp()).split(",")[0].split(":")[1]
								.replaceAll("\"", "");
				if (UUID.contains("The server has not found anything matching the request")) {

				} else {
					String[] names = URLConnectionReader("https://api.mojang.com/user/profiles/" + UUID + "/names")
							.split("name");
					for (int i = 0; i < names.length; i++) {
						if (i == 0) {

						} else {
							String oldName = names[i].split(":")[1];
							oldName = oldName.split(",")[0];
							oldName = oldName.replaceAll("\\{\"", "");
							oldName = oldName.replaceAll("\\p{P}", "");
							if (checkForPastLogin(oldName) && !(oldName.equalsIgnoreCase(player.getDisplayName()))) {
								Main.sendConsole(oldName);
								new DataTransfers(oldName, player.getDisplayName());
							}
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();

			}
		}
	}
	/**
	 * Connects to the website given in the parameter and returns the response as a string (Made for getting UUID's and Name History)
	 * @param website
	 * @return The data sent from the website as a String
	 * @throws IOException
	 */
	public static String URLConnectionReader(String website) throws IOException {
		URL url = new URL(website);
		URLConnection urlConnection = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			return inputLine;
		}
		in.close();
		return null;
	}
	/**
	 * Get the Unix Time Stamp for the current system time
	 * @return The Unix TimeStamp as a long
	 */
	private long getUnixTimeStamp() {
		Date now = new Date();
		Long longTime = new Long(now.getTime() / 1000);
		return longTime;
	}
	/**
	 * Checks the essentials userdata files if that IGN has logged in before
	 * @param username
	 * @return Returns boolean true if the file exists for that username
	 */
	private boolean checkForPastLogin(String username) {
		File folder = new File(System.getProperty("user.dir").replace("\\", "/") + "/plugins/Essentials/userdata/");
		File[] listOfFiles = folder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].getName().equalsIgnoreCase(username + ".yml")) {
				return true;
			}
		}
		return false;
	}
}