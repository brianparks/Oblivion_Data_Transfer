package fish1062.UsernameProject;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

import me.armar.plugins.autorank.Autorank;

public class DataTransfers {
	/**
	 * The Old Name of the Player
	 */
	private String oldName = "";
	/**
	 * The New Name of the Player
	 */
	private String newName = "";
	/**
	 * The Server that this is running on
	 */
	private Server server = null;
	/**
	 * The CommandManagerForPEX to use for PEX data transfers
	 */
	private CommandManagerForPEX commandSenderForPEX = null;
	/**
	 * Constructor to transfer the data from one player to another
	 * @param oldName
	 * @param newName
	 * @param player
	 */
	public DataTransfers(String oldName, String newName) {
		this.oldName = oldName;
		this.newName = newName;
		this.server = Bukkit.getServer();
		this.commandSenderForPEX = new CommandManagerForPEX(server, oldName, newName);
		
		server.dispatchCommand(commandSenderForPEX, "pex user " + oldName);
		server.dispatchCommand(server.getConsoleSender(), "pex user " + oldName + " delete");
		updateEssentials();
		updateWorldData();
		updateFactions();
		updateAR();
		server.dispatchCommand(server.getConsoleSender(), "broadcast &a" + oldName + " Has Changed There IGN to " + newName);
	}
	/**
	 * Updates the Essentials Data for the user
	 */
	private void updateEssentials() {
		renameFile(System.getProperty("user.dir") + "/plugins/Essentials/userdata/"
				+ oldName.toLowerCase() + ".yml", oldName.toLowerCase(), newName.toLowerCase());
	}
	/**
	 * Updates the Factions Data for the user
	 */
	private void updateFactions() {
		renameFile(System.getProperty("user.dir") + "/mstore/factions_uplayer@default/" + oldName
				+ ".json", oldName, newName);
	}
	/**
	 * Updates the World Data for the user
	 */
	private void updateWorldData() {
		renameFile(System.getProperty("user.dir") + "/world/players/" + oldName + ".dat", oldName,
				newName);
	}
	/**
	 * Updates the Autorank Data for the user
	 */
	private void updateAR() {
		Autorank autorank = getAutorank();
		int playTime = autorank.getPlaytimes().getTime(oldName);
		autorank.setTime(newName, playTime);
		autorank.setTime(oldName, 0);
	}
	/**
	 * Gets the Autorank class to be able to use its API
	 * @return The server's Autorank instance
	 */
	private Autorank getAutorank() {
		Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("Autorank");
			
		if (plugin == null || !(plugin instanceof Autorank)) {
			return null;
		}
			
		return (Autorank) plugin;
	}
	/**
	 * Renames the file at the specified Path 
	 * @param pathName
	 * @param oldName
	 * @param newName
	 */
	private void renameFile(String pathName, String oldName, String newName) {
		File file = new File(pathName.replace("\\", "/"));
		file.renameTo(new File(pathName.replace(oldName, newName)));
	}
}
