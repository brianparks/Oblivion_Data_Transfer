package fish1062.UsernameProject;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

public class CommandManagerForPEX implements CommandSender {
	/**
	 * Boolean value that says if ranks are done transfering
	 */
	private boolean ranksDone = false;
	/**
	 * The Old Name of the Player
	 */
	private String oldName = "";
	/**
	 * The New Name of the Player
	 */
	private String newName = "";
	/**
	 * The Server that this is running on
	 */
	private Server server = null;
	/**
	 * Constructor for CommandManagerForPEX
	 * This class was created as a Command sender to process through the data from pex as returned from the command pex user (oldName) and assign that data to the new user and delete the old
	 * @param server
	 * @param oldName
	 * @param newName
	 */
	public CommandManagerForPEX(Server server, String oldName, String newName) {
		this.server = server;
		this.oldName = oldName;
		this.newName = newName;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, int arg1) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2) {
		return null;
	}

	@Override
	public PermissionAttachment addAttachment(Plugin arg0, String arg1, boolean arg2, int arg3) {
		return null;
	}

	@Override
	public Set<PermissionAttachmentInfo> getEffectivePermissions() {
		return null;
	}

	@Override
	public boolean hasPermission(String arg0) {
		return false;
	}

	@Override
	public boolean hasPermission(Permission arg0) {
		return false;
	}

	@Override
	public boolean isPermissionSet(String arg0) {
		return false;
	}

	@Override
	public boolean isPermissionSet(Permission arg0) {
		return false;
	}

	@Override
	public void recalculatePermissions() {

	}

	@Override
	public void removeAttachment(PermissionAttachment arg0) {

	}

	@Override
	public boolean isOp() {
		return false;
	}

	@Override
	public void setOp(boolean arg0) {

	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public Server getServer() {
		return null;
	}

	@Override
	public void sendMessage(String arg0) {
		if (!arg0.contains(oldName)) {
			if (arg0.contains("not ranked") && ranksDone == false) {
				Main.sendConsole("Added " + newName + " To Group " + arg0.replaceAll(" ", "").split("\\(")[0]);
				server.dispatchCommand(Bukkit.getConsoleSender(),
						"pex user " + newName + " group add " + arg0.replaceAll(" ", "").split("\\(")[0]);
			} else if (arg0.contains("not ranked") == false && ranksDone == false) {
				server.dispatchCommand(Bukkit.getConsoleSender(),
						"pex user " + newName + " group add " + arg0.replaceAll(" ", ""));
			}
			if (arg0.contains("(own)")) {
				server.dispatchCommand(Bukkit.getConsoleSender(),
						"pex user " + newName + " add " + arg0.split("\\)")[1].replaceAll(" ", "").replaceAll("\\(own", ""));
			} else {
			}
		}
		else if((!arg0.contains("are member of"))) {
			ranksDone = true;
		}

	}

	@Override
	public void sendMessage(String[] arg0) {

	}

}
