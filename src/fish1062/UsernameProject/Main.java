package fish1062.UsernameProject;

import java.util.logging.Logger;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	public static Logger logger = Logger.getLogger("Minecraft");

	public static void sendConsole(String message) {
		logger.info("Transfer Plugin: " + message);
	}

	public void onEnable() {
		Main.sendConsole("Plugin Enabled");
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}

	public void onDisable() {
		Main.sendConsole("Plugin Disabled");
	}

}
